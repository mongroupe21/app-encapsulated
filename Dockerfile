# syntax=docker/dockerfile:1

FROM php:8.1-fpm
ENV APP_ENV=prod

# Installation des dépendances Linux nécessaire au bon fonctionnement de Symfony
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    git \
    nano \
    libxml2-dev \
    sudo \
    zip \
    unzip \
    && docker-php-ext-install intl opcache

# Installation des extensions PHP pour DB et gestion des images
RUN curl -sSLf \
        -o /usr/local/bin/install-php-extensions \
        https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions && \
    chmod +x /usr/local/bin/install-php-extensions

RUN install-php-extensions pgsql pdo_pgsql gd

# Installation de composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN chmod +x /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

# Installation de symfony-cli
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash && \
    apt install symfony-cli

RUN mkdir -p /var/www/html

WORKDIR /var/www/html

COPY . .

RUN composer install

CMD symfony server:start


